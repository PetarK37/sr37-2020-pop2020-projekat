﻿using SR37_2020_POP2021.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Models
{
    public class Workout
    {
        private int _id;
        private bool _isDeleted;
        private DateTime _date;
        private DateTime _startingTime;
        private int _duration; //in minutes
        private EResevationStatus _status;
        private Instructor _instructor;
        private User _user;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool isDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }


        public DateTime date
        {
            get { return _date; }
            set { _date = value; }
        }
        public DateTime startingTime 
        {
            get { return _startingTime; }
            set { _startingTime = value; }
        }

        public int duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public EResevationStatus status
        {
            get { return _status; }
            set { _status = value; }
        }

             
        public Instructor instructor
        {
            get { return _instructor; }
            set { _instructor = value; }
        }

        public User user
        {
            get { return _user; }
            set { _user = value; }
        }

        public Workout(int id, bool isDeleted, DateTime date, DateTime startingTime, int duration, EResevationStatus status, Instructor instructor, User user)
        {
            this._id = id;
            this._isDeleted = isDeleted;
            this._date = date;
            this._startingTime = startingTime;
            this._duration = duration;
            this._status = status;
            this._instructor = instructor;
            this._user = user;
        }

        public Workout() { }
    }
}