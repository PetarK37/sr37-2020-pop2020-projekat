﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SR37_2020_POP2021.Enums;

namespace SR37_2020_POP2021.Models
{
    public class GenericUser 
    {

        private int _id;
        private bool _isDeleted;
        private String _name;
        private String _userName;
        private String _surname;
        private String _JMBG;
        private EGender _gender;
        private Address _address;
        private String _email;
        private String _password;
        private EUserType _userType;

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool isDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        public String name
        {
            get { return _name; }
            set { _name = value; }
        }

        public String userName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public String surname
        {
            get { return _surname; }
            set { _surname = value; }
        }

        public String JMBG
        {
            get { return _JMBG; }
            set { _JMBG = value; }
        }

        public EGender gender
        {
            get { return _gender; }
            set { _gender = value; }
        }

        public Address address
        {
            get { return _address; }
            set { _address = value; }
        }

        public String email
        {
            get { return _email; }
            set { _email = value; }
        }


        public String password
        {
            get { return _password; }
            set { _password = value; }
        }


        public EUserType userType
        {
            get { return _userType; }
            set { _userType = value; }
        }


        public GenericUser(int id, bool isDeleted, String name, String surname, String JMBG, EGender gender, Address address, String email, String userName, String password,EUserType userType)
        {
            this._id = id;
            this._isDeleted = isDeleted;
            this._name = name;
            this._surname = surname;
            this._JMBG = JMBG;
            this._gender = gender;
            this._address = address;
            this._email = email;
            this._userName = userName;
            this._password = password;
            this._userType = userType;
        }

        public GenericUser()
        {
            this._address = new Address();
            this._userType = EUserType.POLAZNIK;
        }

        public GenericUser Clone()
        {
            GenericUser copy = new GenericUser();
            copy.id = id;
            copy.isDeleted = isDeleted;
            copy.name = name;
            copy.surname = surname;
            copy.JMBG = JMBG;
            copy.gender = gender;
            copy.address = address.Clone();
            copy.email = email;
            copy.userName = userName;
            copy.password = password;
            copy.userType = userType;

            return copy;
        }

        override
        public String ToString()
        {
            return _name + " " + _surname;
        }
    }


}
