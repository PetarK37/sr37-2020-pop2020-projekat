﻿using SR37_2020_POP2021.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Models
{
    public class Admin 
    {
        private GenericUser _baseClass;

        public GenericUser baseClass
        {
            get { return _baseClass; }
            set { _baseClass = value; }
        }

        public Admin(int id, bool isDeleted, String name, String surname, String JMBG, EGender gender, Address address, String email, String userName, String password)
        {
            this._baseClass = new GenericUser(id, isDeleted, name, surname, JMBG, gender, address, email, userName, password,EUserType.ADMINISTRATOR);
        }

        public Admin(GenericUser parent)
        {
            this._baseClass = parent;
        }
        override
        public String ToString()
        {
            return _baseClass.name + " " + _baseClass.surname; 
        }
    }
}
