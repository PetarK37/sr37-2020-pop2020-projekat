﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Models
{
    public class Address 
    {

        private String _street;
        private String _number;
        private String _city;
        private String _country;
        private int _id { get; set; }
        private bool _isDeleted { get; set; }

        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool isDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }

        public String street 
        { 
            get {return _street ;} 
            set{_street = value; } 
        }

        public String number
        {
            get { return _number; }
            set { _number = value; }
        }
        public String city
        {
            get { return _city; }
            set { _city = value; }
        }
        public String country
        {
            get { return _country; }
            set { _country = value; }
        }

        public Address(int id, bool isDeleted, String street, String number, String city, String country) 
        {
            this._id = id;
            this._isDeleted = isDeleted;
            this._street = street;
            this._number = number;
            this._city = city;
            this._country = country;
        }

        public Address()
        {
        }

        override
       public String ToString()
        {
            return _street + " " + _number + " , " + _city + " | " + _country;
        }

        internal Address Clone()
        {
            Address copy = new Address();

            copy.id = id;
            copy.isDeleted = isDeleted;
            copy.city = city;
            copy.street = street;
            copy.number = number;
            copy.country = country;

            return copy;
        }
    }
}
