﻿using SR37_2020_POP2021.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Models
{
    public class Instructor 
    {
        private ObservableCollection<Workout> _instructorWorkouts;
        private GenericUser _baseClass;

        public GenericUser baseClass
        {
            get{ return _baseClass; }
            set { _baseClass = value; }
        }

        public ObservableCollection<Workout> userWorkouts
        {
            get { return _instructorWorkouts; }
            set { _instructorWorkouts = value; }
        }


        public Instructor(int id, bool isDeleted, String name, String surname, String JMBG, EGender gender, Address address, String email, String userName, String password)
        {
            this._baseClass = new GenericUser(id, isDeleted, name, surname, JMBG, gender, address, email, userName, password,EUserType.INSTRUKTOR);
            this._instructorWorkouts = new ObservableCollection<Workout>();

        }

        public Instructor(GenericUser parent)
        {
            this._baseClass = parent;
            this._instructorWorkouts = new ObservableCollection<Workout>();

        }

        public Instructor()
        {
        }

        override
        public String ToString()
        {
            return _baseClass.name + " " + _baseClass.surname;
        }

    }
}
