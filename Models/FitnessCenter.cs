﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Models
{
    public class FitnessCenter
    {
        private int _id;
        private bool _isDeleted;
        private String _name { get; set; }
        private Address _address { get; set; }
     
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool isDeleted
        {
            get { return _isDeleted; }
            set { _isDeleted = value; }
        }
        public String name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Address address
        {
            get { return _address; }
            set { _address = value; }
        }

        public FitnessCenter(int id, bool isDeleted, String name, Address address)
        {
            this._id = id;
            this._isDeleted = isDeleted;
            this._name = name;
            this._address = address;
        }

        public FitnessCenter()
        {
        }

        public FitnessCenter Clone()
        {
            FitnessCenter copy = new FitnessCenter();
            copy.id = id;
            copy.isDeleted = false;
            copy.address = address.Clone();
            copy.name = name;
            return copy;

        }
    }
}
