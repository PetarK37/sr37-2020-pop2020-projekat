﻿using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for AboutUsWindow.xaml
    /// </summary>
    public partial class AboutUsWindow : Window
    {
        FitnessCenter copy = Util.Instance.FitnessCenter.Clone();
        public AboutUsWindow()
        {
            InitializeComponent();
            this.DataContext = Util.Instance.FitnessCenter;
            this.Closing += new CancelEventHandler(AboutUsWindow_Closed);

            if(LoggedInUser.Instance.LoggedUser != null)
            {
                if(LoggedInUser.Instance.LoggedUser.userType is EUserType.ADMINISTRATOR)
                {
                    nameTxtBox.IsReadOnly = false;
                    idTxtBox.IsReadOnly = false;
                    addressNumTextBox.IsReadOnly = false;
                    addressStreetTxtBox.IsReadOnly = false;
                    cancelBtn.Visibility = Visibility.Visible;
                    editBtn.Visibility = Visibility.Visible;

                }
            }
            
        }

        private void AboutUsWindow_Closed(object sender, EventArgs e)
        {
            if(LoggedInUser.Instance.LoggedUser == null)
            {
                new GuestWindow().Show();

            }else if (LoggedInUser.Instance.LoggedUser.userType is EUserType.INSTRUKTOR)
            {
                new InstructorWindow().Show();
            }else if (LoggedInUser.Instance.LoggedUser.userType is EUserType.POLAZNIK)
            {
                new UserWindow().Show();
            }
            else
            {
                new AdminWindow().Show();
            }
        }

        private void editBtn_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid())
            {
            FitnessCenterSQLRepository repository = new FitnessCenterSQLRepository();
            repository.Update(Util.Instance.FitnessCenter);
            this.Close();
            }
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Util.Instance.FitnessCenter = copy;
            this.Close();
        }

        private bool IsValid()
        {
            return !Validation.GetHasError(idTxtBox) && !Validation.GetHasError(nameTxtBox) && !Validation.GetHasError(addressStreetTxtBox) && !Validation.GetHasError(addressNumTextBox);
        }
    }


}
