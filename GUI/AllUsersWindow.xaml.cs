﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System.ComponentModel;
using SR37_2020_POP2021.Controllers;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for AllUsersWindow.xaml
    /// </summary>
    public partial class AllUsersWindow : Window
    {
        ICollectionView view;

        public AllUsersWindow()
        {

            InitializeComponent();
            typeComboBox.ItemsSource = Enum.GetValues(typeof(EUserType)).Cast<EUserType>();
            UpdateView();
            view.Filter = CustomFilter;
             this.Closing += new CancelEventHandler(AllUsersWindow_Closed);
        }

        private void AllUsersWindow_Closed(object sender, EventArgs e)
        {
            new AdminWindow().Show();
        }
    


        private void UpdateView()
        {
            DGAllUsers.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(Util.Instance.GenericUsers);
            DGAllUsers.ItemsSource = view;
            DGAllUsers.IsSynchronizedWithCurrentItem = true;
            DGAllUsers.SelectedItem = null;

            DGAllUsers.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
        }

        private void DGAllUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }else if (e.PropertyName.Equals("password"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            
        }

        private bool CustomFilter(object obj)
        {
            GenericUser user = obj as GenericUser;

            if (nameTextBox.Text != "" || surnameTextBox.Text != "" || emailTextBox.Text != "" || streetTextBox.Text != "" || typeComboBox.SelectedIndex != -1)
            {
                return user.name.ToLower().Contains(nameTextBox.Text.ToLower()) && user.surname.ToLower().Contains(surnameTextBox.Text.ToLower())
                    && user.email.ToLower().Contains(emailTextBox.Text.ToLower()) &&
               (user.address.street.ToLower().Contains(streetTextBox.Text.ToLower()) || user.address.number.ToLower().Contains(streetTextBox.Text.ToLower())) 
               && checkComoboxChanges(user);
            }
            else if(user.isDeleted == true)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool checkComoboxChanges(GenericUser user)
        {
            if(typeComboBox.SelectedIndex == -1)
            {
                return true;
            }else
            {
                return user.userType.Equals(typeComboBox.SelectedValue);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            view.Refresh();
            typeComboBox.SelectedIndex = -1;

        }

        private void delteBtn_Click(object sender, RoutedEventArgs e)
        {

            if (MessageBox.Show("a li zelite da obrisete izbranog korisnika?",
                "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                GenericUser selectedUser = view.CurrentItem as GenericUser;
                if (selectedUser == null)
                {
                    MessageBox.Show("Morate izabrati nesto","", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    UsersController.deleteUser(selectedUser);

                    UpdateView();
                    view.Refresh();
                }
                    
            }

        }

        private void addNewBtn_Click(object sender, RoutedEventArgs e)
        {
            GenericUser newUser = new GenericUser();
            AddEditUserWindow addEditWindow = new AddEditUserWindow(newUser, EDialogStatus.ADD);
            this.Hide();
            if (!(bool)addEditWindow.ShowDialog())
            {

            }
            this.Show();
            DGAllUsers.SelectedItem = null;

        }

        private void editUserBtn_Click(object sender, RoutedEventArgs e)
        {
            GenericUser selectedUser = view.CurrentItem as GenericUser;
            if (selectedUser == null)
            {
                MessageBox.Show("Morate izabrati nesto", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                GenericUser oldUser = selectedUser.Clone();

                AddEditUserWindow addEditWindow = new AddEditUserWindow(selectedUser, EDialogStatus.EDIT);
                this.Hide();
                if (!(bool)addEditWindow.ShowDialog())
                {
                    int index = Util.Instance.GenericUsers.ToList().FindIndex(K => K.id.Equals(selectedUser.id));
                    Util.Instance.GenericUsers[index] = oldUser;
                }
                this.Show();
                view.Refresh();
                DGAllUsers.SelectedItem = null;

            }
        }
    }
 
}
