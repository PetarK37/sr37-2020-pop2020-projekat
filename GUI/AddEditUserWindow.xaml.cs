﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for AddEditUserWindow.xaml
    /// </summary>
    public partial class AddEditUserWindow : Window
    {

        private GenericUser chosenEntity;
        private EDialogStatus chosenStatus;
        public AddEditUserWindow(GenericUser user, EDialogStatus status)
        {
            InitializeComponent();
            this.DataContext = user;

            userTypeComboBox.ItemsSource = Enum.GetValues(typeof(EUserType));
            genderComboBox.ItemsSource = Enum.GetValues(typeof(EGender));


            chosenEntity = user;
            chosenStatus = status;

            if (status is EDialogStatus.EDIT && user != null)
            {
                this.Title = "Izmeni korisnika";

            }else if(status is EDialogStatus.MY_PROFILE && user != null)
            {
                this.Title = "Moj profil";
                titleLabel.Text = "Pregled i izmena vasih podataka";
                JMBGTextBox.IsReadOnly = true;
                JMBGTextBox.IsEnabled = false;
                userTypeComboBox.Visibility = Visibility.Collapsed;
                typeLabel.Visibility = Visibility.Collapsed;
            }
            else
            {
                this.Title = "Dodaj koriniska";
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsValid())
                {
                    if (chosenStatus is EDialogStatus.ADD)
                    {
                        if (chosenEntity.userType is EUserType.INSTRUKTOR)
                        {
                            Util.Instance.Instructors.Add(new Instructor(chosenEntity));
                        }
                        if (chosenEntity.userType is EUserType.ADMINISTRATOR)
                        {
                            Util.Instance.Admins.Add(new Admin(chosenEntity));
                        }
                        else if (chosenEntity.userType is EUserType.POLAZNIK)
                        {
                            Util.Instance.Users.Add(new User(chosenEntity));
                        }

                        UsersController.saveUser(chosenEntity);

                    }
                    else
                    {
                        UsersController.UpdateUser(chosenEntity);
                    }

                    this.DialogResult = true;
                    this.Close();

                }
            } catch (UniqueValueException ex)
            {
                MessageBox.Show("Korisnik sa tim podacima vec postoji u bazi. \n Korisinicko ime,email ili JMB moraju biti jednistveni!", ex.Message, MessageBoxButton.OK, MessageBoxImage.Information);

            }
            catch (EmptyDataException exc)
            {
                MessageBox.Show("Proverite da li ste popunili sva polja!", exc.Message, MessageBoxButton.OK, MessageBoxImage.Information);

            }


        }

        private bool IsValid()
        {
            return !Validation.GetHasError(nemeTextBox) && !Validation.GetHasError(surnameTextBox) && !Validation.GetHasError(usernameTextBox) && !Validation.GetHasError(passwordTextBox)
                && !Validation.GetHasError(emailTextBox) && !Validation.GetHasError(JMBGTextBox) && !Validation.GetHasError(countryTextBox) && !Validation.GetHasError(cityTextBox)
                && !Validation.GetHasError(streetTextBox) && !Validation.GetHasError(houseNumberTextBox);
        }
    }
}
