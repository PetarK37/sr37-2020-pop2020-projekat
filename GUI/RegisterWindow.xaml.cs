﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SR37_2020_POP2021.Exceptions;
using System.ComponentModel;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        private GenericUser chosenEntity;

        public RegisterWindow(GenericUser newUser)
        {

            InitializeComponent();
            this.DataContext = newUser;
            genderComboBox.ItemsSource = Enum.GetValues(typeof(EGender));

            chosenEntity = newUser;
        }

        

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (IsValid())
            {
                try
                {
                    UsersController.SignUp(chosenEntity);


                    this.DialogResult = true;
                    this.Close();
                }catch(UniqueValueException ex)
                {
                    MessageBox.Show( "Korisnik sa tim podacima vec postoji u bazi. \n Korisinicko ime,email ili JMB moraju biti jednistveni!",ex.Message, MessageBoxButton.OK, MessageBoxImage.Information);

                }catch(EmptyDataException exc)
                {
                    MessageBox.Show("Proverite da li ste popunili sva polja!", exc.Message, MessageBoxButton.OK, MessageBoxImage.Information);

                }
            }
    }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private bool IsValid()
        {
            return !Validation.GetHasError(nemeTextBox) && !Validation.GetHasError(surnameTextBox) && !Validation.GetHasError(usernameTextBox) && !Validation.GetHasError(passwordTextBox)
                && !Validation.GetHasError(emailTextBox) && !Validation.GetHasError(JMBGTextBox) && !Validation.GetHasError(countryTextBox) && !Validation.GetHasError(cityTextBox) 
                && !Validation.GetHasError(streetTextBox) && !Validation.GetHasError(houseNumberTextBox);
        }
    }
}
