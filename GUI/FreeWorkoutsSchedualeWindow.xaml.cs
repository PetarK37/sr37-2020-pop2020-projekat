﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for FreeWorkoutsSchedualeWindow.xaml
    /// </summary>
    /// 

    public partial class FreeWorkoutsSchedualeWindow : Window
    {
        ICollectionView view;

        public FreeWorkoutsSchedualeWindow()
        {
            InitializeComponent();
            UpdateView();
            view.Filter = CustomFilter;
           this.Closing += new CancelEventHandler(AllWorkoutsWindow_Closed);
            instructorComboBox.ItemsSource = Util.Instance.Instructors;
        }

        private void AllWorkoutsWindow_Closed(object sender, EventArgs e)
        {

            new UserWindow().Show(); 
        }

        private bool CustomFilter(object obj)
        {
            Workout workout = obj as Workout;

            if (workout.status != Enums.EResevationStatus.SLOBODAN)
            {
                return false;
            }
            if(workout.date < DateTime.Now && workout.startingTime.TimeOfDay < DateTime.Now.TimeOfDay)
            {
                return false;
            }
            if (datePicker.SelectedDate != null)
            {
                DateTime dateFromPicker = (DateTime)datePicker.SelectedDate;
                return workout.date.Date == dateFromPicker.Date;
            }
            if(instructorComboBox.SelectedIndex != -1)
            {
                return workout.instructor.Equals(instructorComboBox.SelectedItem);
            }
            return true;

        }

        private void UpdateView()
        {
            DgFreeWorkouts.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(Util.Instance.Workouts);
            view.SortDescriptions.Add(new SortDescription("name", ListSortDirection.Ascending));
            DgFreeWorkouts.ItemsSource = view;
            DgFreeWorkouts.IsSynchronizedWithCurrentItem = true;
            DgFreeWorkouts.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DgFreeWorkouts.SelectedItem = null;

        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            view.Refresh();
            datePicker.SelectedDate = null;
        }

        private void reserveBtn_Click(object sender, RoutedEventArgs e)
        {
            Workout selectedItem = view.CurrentItem as Workout;
            if (selectedItem == null)
            {
                MessageBox.Show("Morate izabrati nesto", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                selectedItem.user = UsersController.GetUser(LoggedInUser.Instance.LoggedUser.id);
                WorkoutsController.Reserve(selectedItem);
                view.Refresh();
                DgFreeWorkouts.SelectedItem = null;

            }
        }


        private void DgFreeWorkouts_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }else if (e.PropertyName.Equals("user"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("status"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("date"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "yyyy - MM - dd";
            }
            else if (e.PropertyName.Equals("startingTime"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "HH:mm";
            }
        }

  
        private void instructorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            view.Refresh();
        }
    }
}
