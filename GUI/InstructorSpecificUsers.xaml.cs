﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for InstructorSpecificUsers.xaml
    /// </summary>
    /// 

    public partial class InstructorSpecificUsers : Window
    {
        ICollectionView view;

        public InstructorSpecificUsers()
        {
            InitializeComponent();
            DGMyUsers.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            UpdateView();
            view.Filter = CustomFilter;
            this.Closing += new CancelEventHandler(MyUsersWindow_Closed);
        }

        private void MyUsersWindow_Closed(object sender, EventArgs e)
        {
                new InstructorWindow().Show();
        }
        private void UpdateView()
        {

            DGMyUsers.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(Util.Instance.GenericUsers);
            DGMyUsers.ItemsSource = view;
            DGMyUsers.IsSynchronizedWithCurrentItem = true;
            DGMyUsers.SelectedItem = null;

            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            GenericUser user = obj as GenericUser;

            Instructor instructor = UsersController.GetInstrucotr(LoggedInUser.Instance.LoggedUser.id);

            if (user.userType != Enums.EUserType.POLAZNIK)
            {
                return false;
            }

            foreach (Workout w in instructor.userWorkouts)
            {
                if(w.user != null)
                {
                    if (w.user.baseClass.Equals(user))
                    {
                        return true;
                    }
                    
                }
            }

            if (user.isDeleted == true)
            {
                return false;
            }

                return false;
        }

        private void DGMyUsers_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {

            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("password"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            if (e.PropertyName.Equals("userType"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }
    }
}
