﻿using SR37_2020_POP2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for GuestWindow.xaml
    /// </summary>
    public partial class GuestWindow : Window
    {
        public GuestWindow()
        {
            InitializeComponent();
        }

        private void aboutUsBtn_Click(object sender, RoutedEventArgs e)
        {
            new AboutUsWindow().Show();
            this.Visibility = Visibility.Hidden;
        }

        private void allInstructorsBtn_Click(object sender, RoutedEventArgs e)
        {
            new AllInstructorsAndSearchWindow().Show();
            this.Close();
        }

        private void backBtn_Click(object sender, RoutedEventArgs e)
        {
            new LoginWindow().Show();
            this.Close();
        }

        private void signUpBtn_Click(object sender, RoutedEventArgs e)
        {
            GenericUser newUser = new GenericUser();
            RegisterWindow registerWindow = new RegisterWindow(newUser);
            registerWindow.ShowDialog();
        }
    }
}
