﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private GenericUser userDTO = new GenericUser();

        public LoginWindow()
        {

            this.DataContext = userDTO;
            if(Util.Instance.GenericUsers == null)
            {
                Util.Instance.LoadAllEntites();
            }
            InitializeComponent();
        }

        private void guestBtn_Click(object sender, RoutedEventArgs e)
        {
            new GuestWindow().Show();
            this.Close();

           
        }

        private void signUpBtn_Click(object sender, RoutedEventArgs e)
        {
            GenericUser newUser = new GenericUser();
            RegisterWindow registerWindow = new RegisterWindow(newUser);
            this.Hide();

            if (!(bool)registerWindow.ShowDialog())
            {
                
            }
            this.Show();
        }

        private void logInBtn_Click(object sender, RoutedEventArgs e)
        {
            UserWindow userWindow = new UserWindow();
            AdminWindow adminWindow = new AdminWindow();
            InstructorWindow instrucotrWindow = new InstructorWindow();

            userDTO.userName = usernameTxtBox.Text;
            userDTO.password = passwordBox.Password;

            if (UsersController.LogIn(userDTO))
            {
                this.Hide();

                if (LoggedInUser.Instance.LoggedUser.userType  is EUserType.ADMINISTRATOR)
                {
                    adminWindow.Show();
                    this.Close();
                }
                else if (LoggedInUser.Instance.LoggedUser.userType  is EUserType.INSTRUKTOR)
                {
                    instrucotrWindow.Show();
                    this.Close();
                }
                else
                {
                    userWindow.Show();
                    this.Close();

                }   
            }
            else
            {
                MessageBox.Show("Proverite svoje podatke", "Nepostojeci korisnik", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            
        }
    }
}
