﻿using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            InitializeComponent();
        }


        private void MenuItem_Click_MyProfile(object sender, RoutedEventArgs e)
        {
            GenericUser oldUser = LoggedInUser.Instance.LoggedUser.Clone();

            AddEditUserWindow addEditWindow = new AddEditUserWindow(LoggedInUser.Instance.LoggedUser, EDialogStatus.MY_PROFILE);
            this.Hide();
            if (!(bool)addEditWindow.ShowDialog())
            {
                int index = Util.Instance.GenericUsers.ToList().FindIndex(K => K.id.Equals(LoggedInUser.Instance.LoggedUser.id));
                Util.Instance.GenericUsers[index] = oldUser;
                LoggedInUser.Instance.LoggedUser = Util.Instance.GenericUsers[index];
            }
            this.Show();
        }

        private void MenuItem_Click_SignOut(object sender, RoutedEventArgs e)
        {
            LoggedInUser.Instance.LogOut();
            this.Close();
            new LoginWindow().Show();
        }

        private void aboutUsBtn_Click(object sender, RoutedEventArgs e)
        {
           new AboutUsWindow().Show();
            this.Close();
        }

        private void allUsersBtn_Click(object sender, RoutedEventArgs e)
        {
            new AllUsersWindow().Show();
            this.Close();
        }

        private void allWorkoutsBtn_Click(object sender, RoutedEventArgs e)
        {
            new AllWorkoutsWindow().Show();
            this.Close();
        }
    }
}
