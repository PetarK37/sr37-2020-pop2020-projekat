﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for MakeSchedualeDialog.xaml
    /// </summary>
    public partial class MakeSchedualeDialog : Window
    {

        Workout WorkoutDTO;
        public StringBuilder errorMessage;

        public MakeSchedualeDialog()
        {
            InitializeComponent();
            if (LoggedInUser.Instance.LoggedUser.userType.Equals(EUserType.ADMINISTRATOR))
            {
                instructorComboBox.ItemsSource = Util.Instance.Instructors;
            }
            else
            {
                instructorComboBox.Visibility = Visibility.Collapsed;
                instructorLabel.Visibility = Visibility.Collapsed;

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (validate())
            {
                if (LoggedInUser.Instance.LoggedUser.userType.Equals(EUserType.ADMINISTRATOR))
                {

                    try
                    {
                        WorkoutDTO = new Workout();
                        WorkoutDTO.isDeleted = false;
                        WorkoutDTO.date = (DateTime)reservationDateBox.SelectedDate;
                        WorkoutDTO.startingTime = DateTime.Parse(timePickerWorkout.Text);
                        WorkoutDTO.duration = Int32.Parse(durationTextBox.Text);
                        WorkoutDTO.instructor = (Instructor)instructorComboBox.SelectedValue;
                        WorkoutDTO.status = EResevationStatus.SLOBODAN;
                        WorkoutsController.MakeSceduale(WorkoutDTO);
                        this.Close();
                    }
                    catch (DateIsNotFreeException ex)
                    {
                        MessageBox.Show(ex.Message, "Zauzet termin", MessageBoxButton.OK, MessageBoxImage.Information);

                    }
                    catch (FormatException)
                    {
                        MessageBox.Show("Proverite polja za vreme i trajanje", "Pogresan format", MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                }
                else
                {

                    try
                    {
                        WorkoutDTO = new Workout();
                        WorkoutDTO.date = (DateTime)reservationDateBox.SelectedDate;
                        WorkoutDTO.startingTime = DateTime.Parse(timePickerWorkout.Text);
                        WorkoutDTO.duration = Int32.Parse(durationTextBox.Text);
                        WorkoutDTO.instructor = UsersController.GetInstrucotr(LoggedInUser.Instance.LoggedUser.id);
                        WorkoutDTO.status = EResevationStatus.SLOBODAN;
                        WorkoutsController.MakeSceduale(WorkoutDTO);
                        this.Close();
                    }
                    catch (DateIsNotFreeException ex)
                    {
                        MessageBox.Show(ex.Message, "Zauzet termin", MessageBoxButton.OK, MessageBoxImage.Information);

                    }catch(FormatException)
                    {
                        MessageBox.Show("Proverite polja za vreme i trajanje", "Pogresan format", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show(errorMessage.ToString(), "Proverite sledece greske:", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private bool validate()
        {
            errorMessage = new StringBuilder();
            bool retVal = true;
            if (durationTextBox.Text.Trim() == "")
            {
                errorMessage.Append("Morate popuniti trajanje! \n");
                retVal = false;
            }
            if (durationTextBox.Text.Any(char.IsLetter))
            {
                errorMessage.Append("Trajanje ne sme imati slova! \n");
                retVal = false;
            }

            if (timePickerWorkout.Text == null || timePickerWorkout.Text.Trim() == "")
            {
                errorMessage.Append("Morate popuniti vreme \n");
                retVal = false;
            }
            else
            {
                if (timePickerWorkout.Text == null || timePickerWorkout.Text.Trim() == "")
                {
                    errorMessage.Append("Morate popuniti vreme \n");
                    retVal = false;
                }
            }
            if (instructorComboBox.SelectedIndex == -1 && instructorComboBox.Visibility == Visibility.Visible)
            {
                errorMessage.Append("Morate izabrati trenera! \n");
                retVal = false;
            }
            if (reservationDateBox.SelectedDate == null)
            {
                errorMessage.Append("Morate popuniti datum! \n");
                retVal = false;
            }
            else
            {
                DateTime notNullDate = (DateTime)reservationDateBox.SelectedDate;

                if (reservationDateBox.SelectedDate != null && notNullDate.Date < DateTime.Now.Date)
                {
                    errorMessage.Append("Izabrani dan je prosao \n");
                    retVal = false;
                }
            }
            return retVal;
        }
    }
}
