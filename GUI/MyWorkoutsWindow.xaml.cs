﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for MyWorkoutsWindow.xaml
    /// </summary>
    public partial class MyWorkoutsWindow : Window
    {
        ICollectionView view;

        public MyWorkoutsWindow()
        {
            InitializeComponent();

            if (LoggedInUser.Instance.LoggedUser.userType is Enums.EUserType.INSTRUKTOR)
            {
                unreserveBtn.Visibility = Visibility.Collapsed;
            }
            else if (LoggedInUser.Instance.LoggedUser.userType is Enums.EUserType.POLAZNIK)
            {
                deleteBtn.Visibility = Visibility.Collapsed;
                addNewBtn.Visibility = Visibility.Collapsed;
            }
            UpdateView();
            view.Filter = CustomFilter;
            this.Closing += new CancelEventHandler(AllWorkoutsWindow_Closed);
        }

        private void AllWorkoutsWindow_Closed(object sender, EventArgs e)
        {
            if (LoggedInUser.Instance.LoggedUser.userType is Enums.EUserType.INSTRUKTOR)
            {
                new InstructorWindow().Show();
            }
            else
            {
                new UserWindow().Show();
            }
        }

        private bool CustomFilter(object obj)
        {
            Workout workout = obj as Workout;
            if (workout.isDeleted == true)
            {
                return false;
            }
            if(datePicker.SelectedDate != null)
            {
                DateTime dateFromPicker = (DateTime)datePicker.SelectedDate;
                return workout.date.Date == dateFromPicker.Date;
            }

            return true;

        }

        private void UpdateView()
        {
            DGMyWorkouts.ItemsSource = null;

            if (LoggedInUser.Instance.LoggedUser.userType is Enums.EUserType.INSTRUKTOR)
            {
                view = CollectionViewSource.GetDefaultView(UsersController.GetInstrucotr(LoggedInUser.Instance.LoggedUser.id).userWorkouts);

            }
            else
            {
                view = CollectionViewSource.GetDefaultView(UsersController.GetUser(LoggedInUser.Instance.LoggedUser.id).userWorkouts);
            }

            DGMyWorkouts.ItemsSource = view;
            DGMyWorkouts.IsSynchronizedWithCurrentItem = true;
            DGMyWorkouts.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGMyWorkouts.SelectedItem = null;

        }



        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("a li zelite da obrisete izbrani termin ?",
               "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Workout selectedItem = view.CurrentItem as Workout;
                if (selectedItem == null)
                {
                    MessageBox.Show("Morate izabrati nesto","", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    if (selectedItem.status is Enums.EResevationStatus.REZERVISAN)
                    {
                        MessageBox.Show("Ne mozete obrisati zakazan termin!", "Zabranjena radnja", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        WorkoutsController.deleteWorkout(selectedItem);
                    }
                }
            }

            UpdateView();
            view.Refresh();
        }


        private void addNewBtn_Click(object sender, RoutedEventArgs e)
        {

            MakeSchedualeDialog makeScheduale = new MakeSchedualeDialog();
            this.Hide();
            if (!(bool)makeScheduale.ShowDialog())
            {

            }
            view.Refresh();
            this.Show();
            DGMyWorkouts.SelectedItem = null;

        }

        private void DGMyWorkouts_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("date"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "yyyy - MM - dd";
            }
            else if (e.PropertyName.Equals("startingTime"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "HH:mm";
            }
        }

        private void unreserveBtn_Click(object sender, RoutedEventArgs e)
        {
            Workout selectedItem = view.CurrentItem as Workout;
            if (selectedItem == null)
            {
                MessageBox.Show("Morate izabrati nesto", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                WorkoutsController.Unreserve(selectedItem);
                view.Refresh();
                DGMyWorkouts.SelectedItem = null;


            }
        }

        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            view.Refresh();
            datePicker.SelectedDate = null;
        }
    }
}
