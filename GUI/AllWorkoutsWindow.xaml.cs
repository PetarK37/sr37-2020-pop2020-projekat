﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for AllWorkoutsWindow.xaml
    /// </summary>
    public partial class AllWorkoutsWindow : Window
    {

        ICollectionView view;
        public AllWorkoutsWindow()
        {
            InitializeComponent();
            UpdateView();
            view.Filter = CustomFilter;
            this.Closing += new CancelEventHandler(AllWorkoutsWindow_Closed);
        }

        private void AllWorkoutsWindow_Closed(object sender, EventArgs e)
        {
           
                new AdminWindow().Show();
        }

        private void UpdateView()
        {
            DGAllWorkouts.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(Util.Instance.Workouts);
            DGAllWorkouts.ItemsSource = view;
            DGAllWorkouts.IsSynchronizedWithCurrentItem = true;
            DGAllWorkouts.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGAllWorkouts.SelectedItem = null;

        }

        private bool CustomFilter(object obj)
        {
            Workout workout = obj as Workout;
            
            if (workout.isDeleted == true)
            {
                return false;
            }
                return true;
            
        }

        private void addNewBtn_Click(object sender, RoutedEventArgs e)
        {

            MakeSchedualeDialog makeScheduale = new MakeSchedualeDialog();
            this.Hide();
            if (!(bool)makeScheduale.ShowDialog())
            {

            }
            view.Refresh();
            this.Show();
            DGAllWorkouts.SelectedItem = null;


        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("a li zelite da obrisete izbrani termin ?",
               "Potvrda", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Workout selectedItem = view.CurrentItem as Workout;
                if (selectedItem == null)
                {
                    MessageBox.Show("Morate izabrati nesto", "",MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    WorkoutsController.deleteWorkout(selectedItem);

                    UpdateView();
                    view.Refresh();
                }
            }
        }

        private void DGAllWorkouts_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("date"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "yyyy - MM - dd";
            }
            else if (e.PropertyName.Equals("startingTime"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "HH:mm";
            }
        }
    }


}
