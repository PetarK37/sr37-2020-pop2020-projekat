﻿using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for AllInstructorsAndSearchWindow.xaml
    /// </summary>
    public partial class AllInstructorsAndSearchWindow : Window
    {
        ICollectionView view;

        public AllInstructorsAndSearchWindow()
        {
            InitializeComponent();
            DGInstructors.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            UpdateView();
            view.Filter = CustomFilter;
            this.Closing += new CancelEventHandler(AllInstructorsWindow_Closed);
        }

        private void AllInstructorsWindow_Closed(object sender, EventArgs e)
        {
            if (LoggedInUser.Instance.LoggedUser == null)
            {
                new GuestWindow().Show();
            }
            else if (LoggedInUser.Instance.LoggedUser.userType is EUserType.INSTRUKTOR)
            {
                new InstructorWindow().Show();
            }
            else if (LoggedInUser.Instance.LoggedUser.userType is EUserType.POLAZNIK)
            {
                new UserWindow().Show();
            }
            else
            {
                new AdminWindow().Show();
            }
        }

            private void UpdateView()
        {
            DGInstructors.ItemsSource = null;
            view = CollectionViewSource.GetDefaultView(Util.Instance.GenericUsers);
            DGInstructors.ItemsSource = view;
            DGInstructors.IsSynchronizedWithCurrentItem = true;
            DGInstructors.SelectedItem = null;

            view.Filter = CustomFilter;

            if(LoggedInUser.Instance.LoggedUser == null)
            {
                instructorWorkoutsBtn.Visibility = Visibility.Collapsed;
            }
        }

       

        private bool CustomFilter(object obj)
        {
            GenericUser user = obj as GenericUser;

            if (user.userType != Enums.EUserType.INSTRUKTOR)
            {
                return false;
            }
            if (nameTextBox.Text != "" || surnameTextBox.Text != "" || emailTextBox.Text != "" || streetTextBox.Text != "")
            {
                return user.name.ToLower().Contains(nameTextBox.Text.ToLower()) && user.surname.ToLower().Contains(surnameTextBox.Text.ToLower())
                    && user.email.ToLower().Contains(emailTextBox.Text.ToLower()) &&
               (user.address.street.ToLower().Contains(streetTextBox.Text.ToLower()) || user.address.number.ToLower().Contains(streetTextBox.Text.ToLower()));
            }
            
            if (user.isDeleted == true)
            {
                return false;
            }

            else
            {
                return true;
            }
        }

      

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            view.Refresh();

        }

        private void DGInstructors_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("password"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            if (e.PropertyName.Equals("userType"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
        }

        private void instructorWorkoutsBtn_Click(object sender, RoutedEventArgs e)
        {
            GenericUser selectedItem =  view.CurrentItem as GenericUser;
            if (selectedItem == null)
            {
                MessageBox.Show("Morate izabrati nesto","", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else 
            { 
                new InstructorSpeceificWokroutsWindow(selectedItem).Show();
            }
        }
    }
}
