﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SR37_2020_POP2021.GUI
{
    /// <summary>
    /// Interaction logic for InstructorSpeceificWokroutsWindow.xaml
    /// </summary>
    public partial class InstructorSpeceificWokroutsWindow : Window
    {
        ICollectionView view;
        GenericUser instructor;

        public InstructorSpeceificWokroutsWindow(GenericUser entity)
        {
            this.instructor = entity;
            InitializeComponent();
            UpdateView();
            view.Filter = CustomFilter;
        }

        private bool CustomFilter(object obj)
        {
            Workout workout = obj as Workout;
            
            if (workout.isDeleted == true)
            {
                return false;
            }
            if (workout.instructor.baseClass != instructor)
            {
                return false;
            }
            if(workout.date < DateTime.Now && workout.startingTime.TimeOfDay < DateTime.Now.TimeOfDay) 
            {
                return false;
            }
            if (datePicker.SelectedDate != null)
            {
                DateTime dateFromPicker = (DateTime)datePicker.SelectedDate;
                return workout.date.Date == dateFromPicker.Date;
            }

            return true;

        }

        private void UpdateView()
        {
            DGInstructorWorkouts.ItemsSource = null;

            view = CollectionViewSource.GetDefaultView(Util.Instance.Workouts);
            DGInstructorWorkouts.ItemsSource = view;
            DGInstructorWorkouts.IsSynchronizedWithCurrentItem = true;
            DGInstructorWorkouts.ColumnWidth = new DataGridLength(1, DataGridLengthUnitType.Star);
            DGInstructorWorkouts.SelectedItem = null;

        }

        private void DGInstructorWorkouts_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName.Equals("isDeleted"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("id"))
            {
                e.Column.Visibility = Visibility.Collapsed;
            }
            else if (e.PropertyName.Equals("date"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "yyyy - MM - dd";
            }
            else if (e.PropertyName.Equals("startingTime"))
            {
                (e.Column as DataGridTextColumn).Binding.StringFormat = "HH:mm";
            }
        }

        private void unreserveBtn_Click(object sender, RoutedEventArgs e)
        {
            Workout selectedItem = view.CurrentItem as Workout;
            
           if(selectedItem == null)
            {
                MessageBox.Show("Morate izabrati nesto","", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (selectedItem.status != Enums.EResevationStatus.REZERVISAN)
                {
                    MessageBox.Show("Ne mozete otkazati slobodan  termin", "Zauzet termin", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else if (selectedItem.user.baseClass.userName != LoggedInUser.Instance.LoggedUser.userName)
                {
                    MessageBox.Show("Ne mozete otkazati tudji termin", "Zauzet termin", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    WorkoutsController.Unreserve(selectedItem);
                    view.Refresh();
                }
            }
        }

        private void reserveBtn_Click(object sender, RoutedEventArgs e)
        {
            Workout selectedItem = view.CurrentItem as Workout;
            
            if (selectedItem == null)
            {
                MessageBox.Show("Morate izabrati nesto", "", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (selectedItem.status != Enums.EResevationStatus.SLOBODAN)
                {
                    MessageBox.Show("Ne mozete zakazati rezervisan  termin", "Zauzet termin", MessageBoxButton.OK, MessageBoxImage.Information);

                }
                else
                {
                    selectedItem.user = UsersController.GetUser(LoggedInUser.Instance.LoggedUser.id);
                    WorkoutsController.Reserve(selectedItem);
                    view.Refresh();
                    DGInstructorWorkouts.SelectedItem = null;

                }
            }
        }


        private void okBtn_Click(object sender, RoutedEventArgs e)
        {
            view.Refresh();
            datePicker.SelectedDate = null;
        }
    }
}
