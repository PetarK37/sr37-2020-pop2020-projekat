﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Enums
{
    public enum EUserType
    {
        ADMINISTRATOR, POLAZNIK, INSTRUKTOR
    }
}
