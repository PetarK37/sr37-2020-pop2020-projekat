﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SR37_2020_POP2021.Models;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace SR37_2020_POP2021.Repositories
{
    interface IGenericRepository <T>
    {
        void save(T entity);
        void update(T entity);
        void delete(int id);
        bool exists(T entity);

        ObservableCollection<T> Load();

    }
}
