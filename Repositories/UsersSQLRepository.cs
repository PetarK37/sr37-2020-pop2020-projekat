﻿using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

namespace SR37_2020_POP2021.Utilities
{
    public class UsersSQLRepository : IGenericRepository<GenericUser> , IGenericSQLRepository<GenericUser>
    {

        public SqlConnection connection;
        public SqlCommand command;
        public SqlDataReader dataReader;
        public String query;
        SqlDataAdapter adapter = new SqlDataAdapter();


        public void delete(int id)
        {
            MakeConnection();

            query = "Update  Users set  isDeleted = 'true' Where  userId = " + id;
            command = new SqlCommand(query, connection);

            adapter.UpdateCommand = command;
            adapter.UpdateCommand.ExecuteNonQuery();

            CloseConnection();

        }


        public bool exists(GenericUser entity)
        {
            MakeConnection();

            query = "Select * from Users Where isDeleted = 'false' and userName = @username and _password = @password ";
            using (command = new SqlCommand(query, connection))
            {
                command.Parameters.Add(new SqlParameter("username", entity.userName));
                command.Parameters.Add(new SqlParameter("password", entity.password));
                using (dataReader = command.ExecuteReader())
                {
                    if (dataReader.HasRows)
                    {
                        return true;
                    }
                }
            }

            CloseConnection();
            dataReader.Close();

            return false;
        }

        public ObservableCollection<GenericUser> Load()
        {
            ObservableCollection<GenericUser> retList = new ObservableCollection<GenericUser>();
            try
            {

                MakeConnection();

                query = "Select * from Users Where isDeleted = 'false' ";
                command = new SqlCommand(query, connection);
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    retList.Add(new GenericUser(dataReader.GetInt32(0), Convert.ToBoolean(dataReader.GetString(1)), dataReader.GetString(2), dataReader.GetString(3), dataReader.GetString(7), ConvertGender(dataReader.GetString(8)), new Address(), dataReader.GetString(5), dataReader.GetString(4), dataReader.GetString(6), CovertType(dataReader.GetString(9))));
                }

                CloseConnection();
                dataReader.Close();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (InvalidCastException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }

            return AddAdressesToUsers(retList);
        }





        public void save(GenericUser entity)
        {

            try
            {
                MakeConnection();
                query = "insert into Users(IsDeleted,_name,surname,userName,email,_password,JMBG,gender,userType,adressID) OUTPUT inserted.userId " +
                    "Values (@isDeleted,@name,@surname,@userName,@email,@password,@JMBG,@gender,@userType,@addressID)";

                using (command = new SqlCommand(query, connection))
                {

                    command.Parameters.Add(new SqlParameter("isDeleted", entity.isDeleted.ToString().ToLower()));
                    command.Parameters.Add(new SqlParameter("name", entity.name));
                    command.Parameters.Add(new SqlParameter("surname", entity.surname));
                    command.Parameters.Add(new SqlParameter("userName", entity.userName));
                    command.Parameters.Add(new SqlParameter("email", entity.email));
                    command.Parameters.Add(new SqlParameter("password", entity.password));
                    command.Parameters.Add(new SqlParameter("JMBG", entity.JMBG));
                    command.Parameters.Add(new SqlParameter("gender", entity.gender.ToString()));
                    command.Parameters.Add(new SqlParameter("userType", entity.userType.ToString()));
                    command.Parameters.Add(new SqlParameter("addressID", entity.address.id));

                    using (dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            entity.id = dataReader.GetInt32(0);
                        }
                    }
                }
                command.Dispose();
                CloseConnection();
            }catch(SqlException e)
            {
                throw new DataBaseErrorException("Problemi u  bazi", e);
            }
        }

        public void update(GenericUser entity)
        {

            try
            {
                MakeConnection();
                query = "Update  Users set _name = @name, surname = @surname, userName = @userName, email = @email ,_password = @password ,JMBG = @jmbg ,gender = @gender ,userType = @userType   Where  userId = @id ";
                using (command = new SqlCommand(query, connection))
                {

                    command.Parameters.Add(new SqlParameter("name", entity.name));
                    command.Parameters.Add(new SqlParameter("surname", entity.surname));
                    command.Parameters.Add(new SqlParameter("userName", entity.userName));
                    command.Parameters.Add(new SqlParameter("email", entity.email));
                    command.Parameters.Add(new SqlParameter("password", entity.password));
                    command.Parameters.Add(new SqlParameter("JMBG", entity.JMBG));
                    command.Parameters.Add(new SqlParameter("gender", entity.gender.ToString()));
                    command.Parameters.Add(new SqlParameter("userType", entity.userType.ToString()));
                    command.Parameters.Add(new SqlParameter("id", entity.id));

                    adapter.UpdateCommand = command;
                    adapter.UpdateCommand.ExecuteNonQuery();
                }
                CloseConnection();
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi u  bazi", e);
            }
            

        }

        private ObservableCollection<GenericUser> AddAdressesToUsers(ObservableCollection<GenericUser> userList)
        {
            try
            {
                MakeConnection();
                foreach (GenericUser user in userList)
                {
                    query = "Select * from Addresses Where isDeleted = 'false' and addressId = (select adressId from Users where UserId = " + user.id + " )";
                    command = new SqlCommand(query, connection);
                    dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        user.address = new Address(dataReader.GetInt32(0), Convert.ToBoolean(dataReader.GetString(1)), dataReader.GetString(4), dataReader.GetString(5), dataReader.GetString(3), dataReader.GetString(2));
                    }
                    dataReader.Close();
                }

                CloseConnection();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi sa kastovanjem iz baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi u  bazi", e);
            }
            catch (InvalidCastException e)
            {
                throw new DataBaseErrorException("Problemi u  bazi", e);
            }
            return userList;


        }

        public void MakeConnection()
        {
            connection = new SqlConnection(Constants.SQL_CONECTION_STRING);
            connection.Open(); ;
        }
        public void CloseConnection()
        {
            command.Dispose();
            query = "";
            connection.Close();
        }

        private EGender ConvertGender(String gender)
        {
            switch (gender)
            {
                case "MUSKI":
                    return EGender.MUSKI;
                case "ZENSKI":
                    return EGender.ZENSKI;
                case "DRUGO":
                    return EGender.DRUGO;

            }
            return EGender.DRUGO;
        }

        private EUserType CovertType(String type)
        {
            switch (type)
            {
                case "ADMINISTRATOR":
                    return EUserType.ADMINISTRATOR;
                case "POLAZNIK":
                    return EUserType.POLAZNIK;
                case "INSTRUKTOR":
                    return EUserType.INSTRUKTOR;

            }
            return EUserType.POLAZNIK;
        }

    }

}