﻿using System.Data.SqlClient;

namespace SR37_2020_POP2021.Utilities
{
    public interface IGenericSQLRepository<T>
    {
         void MakeConnection();

         void CloseConnection();

    }
}