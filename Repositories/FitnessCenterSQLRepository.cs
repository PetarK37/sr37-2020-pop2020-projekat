﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Utilities;

namespace SR37_2020_POP2021.Repositories
{
    class FitnessCenterSQLRepository : IGenericSQLRepository<FitnessCenter>
    {
        public SqlConnection connection;
        public SqlCommand command;
        public SqlDataReader dataReader;
        public String query;
        SqlDataAdapter adapter = new SqlDataAdapter();



        public void Update(FitnessCenter entity)
        {
            try
            {
                MakeConnection();
                query = "Update  FitnessCenter set _name = @name, companyId = @companyId where companyId =  @companyId";
                using (command = new SqlCommand(query, connection))
                {

                    command.Parameters.Add(new SqlParameter("name", entity.name));
                    command.Parameters.Add(new SqlParameter("companyId", entity.id));

                    adapter.UpdateCommand = command;
                    adapter.UpdateCommand.ExecuteNonQuery();
                }
                CloseConnection();
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi u  bazi", e);
            }
        }

        public FitnessCenter Load()
        {
            FitnessCenter retVal = new FitnessCenter();           
            try
            {

                MakeConnection();

                query = "Select * from FitnessCenter";
                command = new SqlCommand(query, connection);
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                   retVal = new FitnessCenter(dataReader.GetInt32(2),false,dataReader.GetString(1),new Address());
                }

                CloseConnection();
                dataReader.Close();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (InvalidCastException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }

            return AddAdressesToFitnesCentre(retVal);
        }

        private FitnessCenter AddAdressesToFitnesCentre(FitnessCenter entity)
        {
            try
            {
                MakeConnection();
                
                    query = "Select * from Addresses Where isDeleted = 'false' and addressId = (select addressId from FitnessCenter where companyId = " + entity.id + " )";
                    command = new SqlCommand(query, connection);
                    dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        entity.address = new Address(dataReader.GetInt32(0), Convert.ToBoolean(dataReader.GetString(1)), dataReader.GetString(4), dataReader.GetString(5), dataReader.GetString(3), dataReader.GetString(2));
                    }
                    dataReader.Close();

                CloseConnection();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (InvalidCastException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            return entity;
        }

        public void MakeConnection()
        {
            connection = new SqlConnection(Constants.SQL_CONECTION_STRING);
            connection.Open(); ;
        }
        public void CloseConnection()
        {
            command.Dispose();
            query = "";
            connection.Close();
        }
    }
}
