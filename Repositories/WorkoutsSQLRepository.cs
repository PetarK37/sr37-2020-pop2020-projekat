﻿using SR37_2020_POP2021.Controllers;
using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace SR37_2020_POP2021.Utilities
{
    public class WorkoutsSQLRepository : IGenericRepository<Workout>, IGenericSQLRepository<Workout>
    {

        public SqlConnection connection;
        public SqlCommand command;
        public SqlDataReader dataReader;
        public String query;
        SqlDataAdapter adapter = new SqlDataAdapter();

        public void delete(int id)
        {
            try
            {

                MakeConnection();

                query = "Update  Workouts set  isDeleted = 'true' Where WorkoutId = " + id;
                command = new SqlCommand(query, connection);

                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();

                CloseConnection();

            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri izmeni  baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri izmeni iz baze", e);
            }
            

        }
    

        public void deleteUserWorkouts(String paramName, int id)
        {

            try { 
            
            MakeConnection();

            query = "Update  Workouts set  isDeleted = 'true' Where " + paramName + " = " + id;
            command = new SqlCommand(query, connection);

            adapter.UpdateCommand = command;
            adapter.UpdateCommand.ExecuteNonQuery();

                CloseConnection();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri izmeni  baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri izmeni iz baze", e);
            }
            
        }



        public bool exists(Workout entity)
        {
            throw new System.NotImplementedException();
        }

        public ObservableCollection<Workout> Load()
        {
            ObservableCollection<Workout> retList = new ObservableCollection<Workout>();
            try
            {

                MakeConnection();

                query = "Select * from Workouts Where isDeleted = 'false' ";
                command = new SqlCommand(query, connection);
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    retList.Add(new Workout(dataReader.GetInt32(0), Convert.ToBoolean(dataReader.GetString(1)), dataReader.GetDateTime(2), dataReader.GetDateTime(3), dataReader.GetInt32(4), CovertStatus(dataReader.GetString(5)),UsersController.GetInstrucotr(dataReader.GetInt32(6)),CheckUser(dataReader,7)));
                }

                CloseConnection();
                dataReader.Close();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (InvalidCastException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }

            return retList;
        }

        public void save(Workout entity)
        {

            MakeConnection();
            query = "insert into Workouts(IsDeleted,workoutDate,workoutStartingTime,duration,reservationStatus,instructorId,userId) OUTPUT inserted.WorkoutId " +
                "Values (@isDeleted,@workoutDate,@workoutStartingTime,@duration,@reservationStatus,@instructorId,@userId)";

            using (command = new SqlCommand(query, connection))
            {
                command.Parameters.Add(new SqlParameter("isDeleted", entity.isDeleted.ToString().ToLower()));
                command.Parameters.Add(new SqlParameter("workoutDate", entity.date.ToString(Constants.DATETIME_TO_STR_STRING)));
                command.Parameters.Add(new SqlParameter("workoutStartingTime", entity.startingTime.ToString(Constants.TIME_TO_STR_STRING)));
                command.Parameters.Add(new SqlParameter("duration", entity.duration));
                command.Parameters.Add(new SqlParameter("reservationStatus", entity.status.ToString()));
                command.Parameters.Add(new SqlParameter("instructorId", entity.instructor.baseClass.id));
                command.Parameters.Add(new SqlParameter("userId", DBNull.Value)); //because there is never a user when stroing a new workout 

                using (dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        entity.id = dataReader.GetInt32(0);
                    }
                }
            }
            command.Dispose();
            CloseConnection();
        }

        public void update(Workout entity)
        {
            try
            {

                MakeConnection();

                query = "Update  Workouts set  reservationStatus = @status , userID = @id Where WorkoutId = " + entity.id;

                using (command = new SqlCommand(query, connection))
                {

                    command.Parameters.Add(new SqlParameter("id", (entity.user == null) ? (object)DBNull.Value : entity.user.baseClass.id));
                    command.Parameters.Add(new SqlParameter("status", entity.status.ToString()));

                    adapter.UpdateCommand = command;
                    adapter.UpdateCommand.ExecuteNonQuery();
                }
                CloseConnection();

            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri izmeni  baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri izmeni iz baze", e);
            }
        }

        public void MakeConnection()
        {
            connection = new SqlConnection(Constants.SQL_CONECTION_STRING);
            connection.Open(); ;
        }
        public void CloseConnection()
        {
            command.Dispose();
            query = "";
            connection.Close();
        }

        private User CheckUser(SqlDataReader reader, int index)
        {
            if (!reader.IsDBNull(index))
                return UsersController.GetUser(reader.GetInt32(index));
            return null;
        }
        private EResevationStatus CovertStatus(String status)
        {
            switch (status)
            {
                case "REZERVISAN":
                    return EResevationStatus.REZERVISAN;
                case "SLOBODAN":
                    return EResevationStatus.SLOBODAN;

            }
            return EResevationStatus.SLOBODAN;
        }

    }
}