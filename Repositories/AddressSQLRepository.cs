﻿using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;

namespace SR37_2020_POP2021.Utilities
{
    public class AddressSQLRepository : IGenericRepository<Address> , IGenericSQLRepository<Address>
    {

        public SqlConnection connection;
        public SqlCommand command;
        public SqlDataReader dataReader;
        public String query;
        SqlDataAdapter adapter = new SqlDataAdapter();

        public void delete(int id)
        {
            MakeConnection();

            query = "Delete from Addresses where addressId  = " + id;
            command = new SqlCommand(query, connection);

            adapter.UpdateCommand = command;
            adapter.UpdateCommand.ExecuteNonQuery();

            CloseConnection();
        }

        public bool exists(Address entity)
        {
            throw new System.NotImplementedException();
        }

        public ObservableCollection<Address> Load()
        {
            ObservableCollection<Address> retList = new ObservableCollection<Address>();
            try
            {
                MakeConnection();


                query = "Select * from Addresses Where isDeleted = 'false'  ";
                command = new SqlCommand(query, connection);
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    retList.Add(new Address(dataReader.GetInt32(0), Convert.ToBoolean(dataReader.GetString(1)), dataReader.GetString(4), dataReader.GetString(5), dataReader.GetString(3), dataReader.GetString(2)));
                }
                dataReader.Close();


                CloseConnection();
            }
            catch (InvalidOperationException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            catch (InvalidCastException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }
            return retList;

        }

        public void save(Address entity)
        {
            try
            {
                MakeConnection();
                query = "insert into Addresses (IsDeleted,country,city,street,number) OUTPUT inserted.addressId " +
                    "Values (@isDeleted,@country,@city,@street,@number)";

                using (command = new SqlCommand(query, connection))
                {
                    command.Parameters.Add(new SqlParameter("isDeleted", entity.isDeleted.ToString().ToLower()));
                    command.Parameters.Add(new SqlParameter("country", entity.country));
                    command.Parameters.Add(new SqlParameter("city", entity.city));
                    command.Parameters.Add(new SqlParameter("street", entity.street));
                    command.Parameters.Add(new SqlParameter("number", entity.number));

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            entity.id = reader.GetInt32(0);
                        }
                    }

                }

                command.Dispose();
                CloseConnection();
            }
            catch (SqlException e)
            {
                throw new DataBaseErrorException("Problemi pri ucitavanju iz baze", e);
            }

        }

        public void update(Address entity)
        {
            MakeConnection();
            query = "Update  Addresses set country = @country ,city = @city ,street = @street, number= @number  Where  addressId = @id ";
            using (command = new SqlCommand(query, connection))
            {

                command.Parameters.Add(new SqlParameter("city", entity.city));
                command.Parameters.Add(new SqlParameter("country", entity.country));
                command.Parameters.Add(new SqlParameter("street", entity.street));
                command.Parameters.Add(new SqlParameter("number", entity.number));
                command.Parameters.Add(new SqlParameter("id", entity.id));

                adapter.UpdateCommand = command;
                adapter.UpdateCommand.ExecuteNonQuery();
            }
            CloseConnection();
        }

        public void MakeConnection()
        {
            connection = new SqlConnection(Constants.SQL_CONECTION_STRING);
            connection.Open(); ;
        }
        public void CloseConnection()
        {
            command.Dispose();
            query = "";
            connection.Close();
        }
    }

}