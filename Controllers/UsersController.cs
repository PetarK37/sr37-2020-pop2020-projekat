﻿using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Exceptions;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using SR37_2020_POP2021.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Controllers
{
    class UsersController
    {

        private static UsersSQLRepository usersRepository = new UsersSQLRepository();
        private static WorkoutsSQLRepository workoutsRepository = new WorkoutsSQLRepository();
        private static AddressSQLRepository addressRepostiroy = new AddressSQLRepository();



        public static void deleteUser(GenericUser user)
        {
            usersRepository.delete(user.id);
            
            if (user.userType.Equals(EUserType.INSTRUKTOR))
            {
                workoutsRepository.deleteUserWorkouts("instructorId", user.id);
                WorkoutsController.deleteUserWorkouts(user);

            }
            else if(user.userType.Equals(EUserType.POLAZNIK))
            {
                workoutsRepository.deleteUserWorkouts("userId", user.id);
                WorkoutsController.deleteUserWorkouts(user);

            }
            user.isDeleted = true;
        }

        public static void saveUser(GenericUser user)
        {
            try
            {
            addressRepostiroy.save(user.address);
            usersRepository.save(user);
            Util.Instance.GenericUsers.Add(user);
            }catch(DataBaseErrorException e)
            {
                addressRepostiroy.delete(user.address.id);
                if (!e.InnerException.Message.Contains("UNIQUE "))
                {
                    throw new EmptyDataException("Neke vrednosti su prazne");
                }
                if (e.InnerException.Message.Contains("UNIQUE "))
                {
                    throw new UniqueValueException("Korisnicko ime,email ili JMBG vec postoje u bazi");
                }

            }
        }

        public static void UpdateUser(GenericUser user)
        {
            try
            {
                addressRepostiroy.update(user.address);
                usersRepository.update(user);
            }
            catch (DataBaseErrorException e)
            {
                if (e.InnerException.Message.Contains("UNIQUE KEY"))
                {
                    throw new UniqueValueException("Korisnicko ime,email ili JMBG vec postoje u bazi");
                }
            }
            
        }

        public static bool LogIn(GenericUser userDTO)
        {
            if (usersRepository.exists(userDTO))
            {
                int index = Util.Instance.GenericUsers.ToList().FindIndex(K => K.userName.Equals(userDTO.userName));
                LoggedInUser.Instance.LogIn(Util.Instance.GenericUsers[index]);
                return true;

            }
            return false;
        }

        public static void SignUp(GenericUser userDTO)
        {
            saveUser(userDTO);
            Util.Instance.Users.Add(new User(userDTO));

        }

        public static Instructor GetInstrucotr(int id)
        {
            int index = Util.Instance.Instructors.ToList().FindIndex(K => K.baseClass.id.Equals(id));
            return Util.Instance.Instructors[index];
        }


        public static User GetUser(int id)
        {
            int index = Util.Instance.Users.ToList().FindIndex(K => K.baseClass.id.Equals(id));
            return Util.Instance.Users[index];
        }

    }
}
