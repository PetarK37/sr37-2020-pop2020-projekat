﻿using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using SR37_2020_POP2021.Utilities;
using SR37_2020_POP2021.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SR37_2020_POP2021.Controllers
{


    public static class WorkoutsController
    {
                private static UsersSQLRepository usersRepository = new UsersSQLRepository();
                private static WorkoutsSQLRepository workoutsRepository = new WorkoutsSQLRepository();

        public static void deleteUserWorkouts(GenericUser user)
        {
            foreach (Workout w in Util.Instance.Workouts)
            {
                if (w.instructor.Equals(user))
                {
                    w.isDeleted = true;
                }
                else if (w.user != null)
                {
                    if (w.user.Equals(user)){ 
                    w.isDeleted = true;
                } }
            }
        }

        internal static void deleteWorkout(Workout workout)
        {
            workout.isDeleted = true;
            workoutsRepository.delete(workout.id);
            workout.instructor.userWorkouts.Remove(workout);
        }

        public static void MakeSceduale(Workout WorkoutDTO)
        {
            if (DateIsFree(WorkoutDTO))
            {
                Util.Instance.Workouts.Add(WorkoutDTO);
                WorkoutDTO.instructor.userWorkouts.Add(WorkoutDTO);
                workoutsRepository.save(WorkoutDTO);
            }
            else
            {
                throw new DateIsNotFreeException("Termin je zauzet, izaberite drugi termin");
            }
        }

        public static void Unreserve(Workout WorkoutDTO)
        {
           
                WorkoutDTO.status = EResevationStatus.SLOBODAN;
                WorkoutDTO.user.userWorkouts.Remove(WorkoutDTO);
                WorkoutDTO.user = null;
                workoutsRepository.update(WorkoutDTO);
            
        }

        public static void Reserve(Workout WorkoutDTO)
        {
            WorkoutDTO.status = EResevationStatus.REZERVISAN;
            WorkoutDTO.user.userWorkouts.Add(WorkoutDTO);
            WorkoutDTO.instructor.userWorkouts.Add(WorkoutDTO);
            workoutsRepository.update(WorkoutDTO);

        }


        private static bool DateIsFree(Workout WorkoutDTO)
        {
            foreach (Workout w in WorkoutDTO.instructor.userWorkouts)
            {
                if (w.date.Date == WorkoutDTO.date.Date)
                {
                    if (w.startingTime.TimeOfDay <= WorkoutDTO.startingTime.TimeOfDay && w.startingTime.AddMinutes(w.duration).TimeOfDay >= WorkoutDTO.startingTime.TimeOfDay)
                    {
                        return false;
                    }else if (w.startingTime.TimeOfDay <= WorkoutDTO.startingTime.AddMinutes(WorkoutDTO.duration).TimeOfDay && w.startingTime.AddMinutes(w.duration).TimeOfDay >= WorkoutDTO.startingTime.AddMinutes(WorkoutDTO.duration).TimeOfDay)
                    {
                        return false;
                    }else if(w.startingTime.TimeOfDay < WorkoutDTO.startingTime.TimeOfDay && w.startingTime.AddMinutes(w.duration).TimeOfDay > WorkoutDTO.startingTime.AddMinutes(WorkoutDTO.duration).TimeOfDay)
                    {
                        return false;
                    }
                }

            }

            return true;
        }
    }
}
