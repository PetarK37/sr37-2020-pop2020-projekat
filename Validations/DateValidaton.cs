﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR37_2020_POP2021.Validations
{
    class DateValidaton : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {

            if ((DateTime) value == null)
            {
                return new ValidationResult(false, "Morate popuniti datum!");

            }
            else if ((DateTime) value < new DateTime().Date)
            {
                return new ValidationResult(false, "Izabrani dan je prosao");

            }
            return new ValidationResult(true, "");
        }
    }
}
