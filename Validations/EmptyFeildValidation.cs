﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR37_2020_POP2021.Validations
{
    class EmptyFeildValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString().Trim().Equals(""))
            {
                return new ValidationResult(false, "Polje ne sme biti prazno!");

            }
            else
            {
                return new ValidationResult(true, "");
            }
        }
    }
}
