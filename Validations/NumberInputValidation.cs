﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR37_2020_POP2021.Validations
{
    class NumberInputValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (Int32.Parse(value.ToString()) == 0)
            {
                return new ValidationResult(false, "Morate popuniti polje!");

            }
            else if (value.ToString().Any(char.IsLetter))
            {
                return new ValidationResult(false, "Polje ne sme imati slova!");
            }else if(Int32.Parse(value.ToString()) < 0)
                {
                    return new ValidationResult(false, "Broj ne moze biti negativan");

                }
            return new ValidationResult(true, "");
        }
    }
}
