﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SR37_2020_POP2021.Validations
{
    class JmbgValidation : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (value.ToString().Trim().Equals(""))
            {
                return new ValidationResult(false, "Polje ne sme biti prazno!");

            }
            else if (value.ToString().Length != 13 )
            {
                return new ValidationResult(false, "JMBG mora imati 13 karaktera");

            }
            else if (value.ToString().Any(char.IsLetter)){
                return new ValidationResult(false, "JMBG ne sme imati slova(13 cifara)");
            }
              return  new ValidationResult(true, "");
            
        }
    }
}
