﻿using SR37_2020_POP2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Utilities
{
    public sealed class LoggedInUser
    {
        private static readonly LoggedInUser _instance = new LoggedInUser();
        public GenericUser LoggedUser { get; set; }

        private LoggedInUser() {
            this.LoggedUser = null;
        }

        public static LoggedInUser Instance
        {
            get { return _instance; }
        }

        public void LogIn(GenericUser user)
        {
            this.LoggedUser = user;
        }

        public void LogOut()
        {
            this.LoggedUser = null;
        }


    }
}
