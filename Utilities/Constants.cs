﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Utilities
{
    public static class Constants
    {
        public const String SQL_CONECTION_STRING = @"Data Source = DESKTOP-SFRMP4P\SQLEXPRESS; Initial Catalog = SR37_2020_FitnessCentreDataBase;  Integrated Security=True";
        public const String DATETIME_TO_STR_STRING = "yyyy-MM-dd";
        public const String TIME_TO_STR_STRING = "HH:mm";

    }
}
