﻿using SR37_2020_POP2021.Enums;
using SR37_2020_POP2021.Models;
using SR37_2020_POP2021.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Utilities
{
   public sealed class Util
    {
        private static readonly Util _instance = new Util() { };

        private IGenericRepository<GenericUser> usersRepository;
        private IGenericRepository<Workout> workoutsRepository;
        private FitnessCenterSQLRepository centreRepository;
        public FitnessCenter FitnessCenter { get; set; }
        public ObservableCollection<GenericUser> GenericUsers { get; set; }
        public ObservableCollection<Instructor> Instructors { get; set; }
        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<Admin> Admins { get; set; }



        public ObservableCollection<Workout> Workouts { get; set; }


        private Util()
        {
            usersRepository = new UsersSQLRepository();
            workoutsRepository = new WorkoutsSQLRepository();
            centreRepository = new FitnessCenterSQLRepository();
            Users = new ObservableCollection<User>();
            Admins = new ObservableCollection<Admin>();
            Instructors = new ObservableCollection<Instructor>();

        }

        static Util() { }


        public static Util Instance
        {
            get { return _instance; }
        }

        public void LoadAllEntites()
        {
            this.GenericUsers = usersRepository.Load();
            orderGenericUsers();
            this.Workouts = workoutsRepository.Load();
            orderWorkouts();
            this.FitnessCenter = centreRepository.Load();
        }


        private void orderGenericUsers()
        {
            foreach(GenericUser u in GenericUsers)
            {
                if (u.userType is EUserType.ADMINISTRATOR)
                {
                    Admins.Add(new Admin(u));
                }
                else if (u.userType is EUserType.INSTRUKTOR)
                {
                    Instructors.Add(new Instructor(u));

                }
                else
                {
                    Users.Add(new User(u));

                }
            }
        }

        private void orderWorkouts()
        {
            foreach(Instructor i in Instructors)
            {
                foreach(Workout w in Workouts)
                {
                    if(w.instructor == i)
                    {
                        i.userWorkouts.Add(w);
                    }
                }

            }

            foreach (User u in Users)
            {
                foreach (Workout w in Workouts)
                {
                    if (w.user == u && w.user != null)
                    {
                        u.userWorkouts.Add(w);
                    }
                }

            }
        }

    }
}
