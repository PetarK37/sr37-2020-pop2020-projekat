﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Exceptions
{
    class UniqueValueException : Exception
    {
        public UniqueValueException() { }


        public UniqueValueException(string message) : base(message) { }


        public UniqueValueException(string message, Exception innerException) : base(message, innerException) { }

    }
}
    
    
