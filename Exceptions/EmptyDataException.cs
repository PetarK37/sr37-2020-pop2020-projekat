﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Exceptions
{
    class EmptyDataException : Exception
    {

        public EmptyDataException()
        {
        }

        public EmptyDataException(string message) : base(message)
        {
        }

        public EmptyDataException(string message, Exception innerException) : base(message, innerException)
        { 
        }
    }
}
