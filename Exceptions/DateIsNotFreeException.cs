﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Exceptions
{
    class DateIsNotFreeException : Exception
    {

        public DateIsNotFreeException()
        {
        }

        public DateIsNotFreeException(string message) : base(message)
        {
        }

        public DateIsNotFreeException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
