﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SR37_2020_POP2021.Exceptions
{
    class InvalidChoiceException : Exception
    {
        public InvalidChoiceException()
        {
        }

        public InvalidChoiceException(string message) : base(message)
        {
        }

        public InvalidChoiceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidChoiceException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
